const express = require("express")
const mongoose = require("mongoose")
const config = require("config")
let cors = require('cors');

const app = express()
app.use(express.json({ extended: true }))
app.use("/api/auth", require("./routes/auth_routes"))
app.use("/api/books", require("./routes/books_routes"))
app.use(cors());

const PORT = config.get("port") || 5000

async function start() {
  try {
    await mongoose.connect(config.get("mongoUri"), {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useCreateIndex: true,
    })
    app.listen(PORT, () => console.log(`app started on ${PORT}`))
  } catch (e) {
    console.log("Server error", e.message)
    process.exit(1)
  }
}
start()
