import { useState, useCallback, useEffect } from "react"
import React from "react"
import { useHttp } from "../../hooks/httpHook"
import Loader from "../../components/Loader/Loader"
import { Grid, Typography, Box } from "@material-ui/core"
import Book from "./components/Book"
import BasicPagination from "../../components/BasicPagination/BasicPagination"
import Filter from "./components/Filter"

export const Books = () => {
  const { request, loading } = useHttp()
  const [data, setData] = useState([])
  const [pageNumber, setPage] = useState(1)
  const [pageLimit, setpageLimit] = useState(12)
  const [serchedBookName, setSerchedBookName] = useState("")
  const [currentBooks, setcurrentBooks] = useState([])

  const getBook = useCallback(async () => {
    try {
      const data = await request(`/api/books`, "GET", null)
      setData(data)
      setcurrentBooks(data)
      console.log('data'+data)
    } catch (e) {}
  }, [request])

  useEffect(() => {
    getBook()
  }, [getBook])
 

  const handlePageChange = (pageNumber) => {
    setPage(Number(pageNumber))
  }

  const handleSelectAmount = (event) => {
    console.log(event.target.value)
    const itemsOnPage = Number(event.target.value)
    setpageLimit(itemsOnPage)
  }

  const handleSearchBook = (event) => {
    const bookName = event.target.value
    
    const newData = data.filter((book) =>
      book.title.toLowerCase().includes(bookName.toLowerCase())
    )
    setSerchedBookName(bookName)
    setcurrentBooks(newData)
  }
  const startPoint = (pageNumber - 1) * pageLimit
  const endPoint = startPoint + pageLimit
  const currentData = currentBooks.slice(startPoint, endPoint)
  const totalPages = Math.ceil(currentBooks.length / pageLimit)
  // const handleDeleteBook = (id) => {
  //   const { currentBooks } = this.state
  //   const itemDeleted = currentBooks.find((book) => book.ID === id)
  //   const newData = currentBooks.slice().filter((book) => book !== itemDeleted)
  //   console.log(newData, id, itemDeleted)
  //   this.setState({
  //     currentBooks: newData,
  //   })
  //   deleteBook(id).then((res) => {
  //     // console.log(res)
  //     // console.log(res.data)
  //   })
  // }
 console.log('currentBooks'+currentBooks)
  return (
    <>
      {loading ? (
        <Loader />
      ) : (
        <div>
           <Box m={3}>
          <Filter
            pageLimit={pageLimit}
            onSelectAmount={(e)=>{handleSelectAmount(e)}}
            onSearchBook={(e)=>{handleSearchBook(e)}}
            error={currentData.length === 0 ? true : false}
          />
          {currentData.length > 0 && (
            <>
              <Grid container spacing={3}>
                {currentData.map((book) => (
                  <Grid key={book._id} item lg={4} md={6} sm={6} xs={12}>
                    <Book book={book}  />
                  </Grid>
                ))}
              </Grid>
              <BasicPagination
                onPageChange={handlePageChange}
                totalPages={totalPages}
              />
            </>
          )}
          {data.length > 0 && currentData.length === 0 && (
            <>
              <Typography variant="h5" align="center" gutterBottom>
                Books with name '{serchedBookName}' not found
              </Typography>
              <Typography variant="subtitle1" align="center" gutterBottom>
                Enter please another one.
              </Typography>
            </>
          )}
        </Box>
       
        </div>
      )}
    </>
  
  )
}
