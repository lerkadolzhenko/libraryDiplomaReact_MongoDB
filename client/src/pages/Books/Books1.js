import React, { useCallback } from "react"
import { Grid, Box, Typography } from "@material-ui/core"
import { getBooks, deleteBook } from "../../api/books"
import Book from "./components/Book"
import Loader from "../../components/Loader/Loader"
import BasicPagination from "../../components/BasicPagination/BasicPagination"
import Filter from "./components/Filter"
import Axios from "axios"


export default class Books extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      isLoading: true,
      pageLimit: 12,
      pageNumber: 1,
      serchedBookName: "",
      currentBooks: [],
    }
  }

  componentDidMount() {
    Axios.get('http://localhost:5000/api/books')
            .then((res) => {
                console.log(res.data)
            }).catch((error) => {
                console.log(error)
            });
  }

  handlePageChange = (pageNumber) => {
    this.setState({
      pageNumber: Number(pageNumber),
    })
  }

  handleSelectAmount = (event) => {
    const itemsOnPage = Number(event.target.value)
    this.setState({
      pageLimit: itemsOnPage,
    })
  }

  handleSearchBook = (event) => {
    const bookName = event.target.value
    const { data } = this.state
    const newData = data.filter((book) =>
      book.Title.toLowerCase().includes(bookName.toLowerCase())
    )
    this.setState({
      serchedBookName: bookName,
      currentBooks: newData,
    })
  }

  handleDeleteBook = (id) => {
    const { currentBooks } = this.state
    const itemDeleted = currentBooks.find((book) => book.ID === id)
    const newData = currentBooks.slice().filter((book) => book !== itemDeleted)
    console.log(newData, id, itemDeleted)
    this.setState({
      currentBooks: newData,
    })
    deleteBook(id).then((res) => {
      // console.log(res)
      // console.log(res.data)
    })
  }


  render() {
    const {
      data,
      isLoading,
      pageLimit,
      pageNumber,
      serchedBookName,
      currentBooks,
    } = this.state
    console.log(data)
    const startPoint = (pageNumber - 1) * pageLimit
    const endPoint = startPoint + pageLimit
    const currentData = currentBooks.slice(startPoint, endPoint)
    const totalPages = Math.ceil(currentBooks.length / pageLimit)

    return (
      <div>
        {isLoading ? (
          <Loader />
        ) : (
          <Box m={3}>
            <Filter
              pageLimit={pageLimit}
              onSelectAmount={this.handleSelectAmount}
              onSearchBook={this.handleSearchBook}
              error={currentData.length === 0 ? true : false}
            />
            {currentData.length > 0 && (
              <>
                <Grid container spacing={3}>
                  {currentData.map((book) => (
                    <Grid key={book.ID} item lg={4} md={6} sm={6} xs={12}>
                      <Book book={book} onDeleteBook={this.handleDeleteBook} />
                    </Grid>
                  ))}
                </Grid>
                <BasicPagination
                  onPageChange={this.handlePageChange}
                  totalPages={totalPages}
                />
              </>
            )}
            {data.length > 0 && currentData.length === 0 && (
              <>
                <Typography variant="h5" align="center" gutterBottom>
                  Books with name '{serchedBookName}' not found
                </Typography>
                <Typography variant="subtitle1" align="center" gutterBottom>
                  Enter please another one.
                </Typography>
              </>
            )}
          </Box>
        )}
      </div>
    )
  }
}

export { Books }
