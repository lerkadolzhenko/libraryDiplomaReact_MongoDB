import React from "react"
import { MenuItem, TextField } from "@material-ui/core"

export default function SelectAmount({ onSelectAmount, pageLimit }) {
  return (
    <TextField
      fullWidth
      id="outlined-select-amount"
      select
      label="books on page"
      value={pageLimit}
      onChange={onSelectAmount}
      variant="outlined">
      <MenuItem value={6}>6 books</MenuItem>
      <MenuItem value={12}>12 books</MenuItem>
      <MenuItem value={18}>18 books</MenuItem>
      <MenuItem value={30}>30 books</MenuItem>
      <MenuItem value={60}>60 books</MenuItem>
    </TextField>
  )
}

export { SelectAmount }
