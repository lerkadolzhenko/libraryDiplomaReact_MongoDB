import React from "react"
import { TextField } from "@material-ui/core"

export default function Search({ onSearchBook, error,value }) {
  return (
    <TextField
      error={error}
      fullWidth
      id="search"
      variant="outlined"
      label="Search book"
      type="search"
      value={value}
      onChange={onSearchBook}
    />
  )
}

export { Search }
