import React from "react"
import {
  Card,
  CardContent,
  Typography,
  CardActions,
  Button,
  makeStyles,
  CardMedia,
} from "@material-ui/core"
import { Link } from "react-router-dom"

const useStyles = makeStyles({
  root: {
    height: 120,
  },
  media: {
    height: 300,
  },
})

export default function Book({ book, onDeleteBook }) {
  const classes = useStyles()
  return (
    <Card >
      <Link to={`/books/${book._id}`} style={{ textDecoration: "none" }}>
        <CardMedia
          className={classes.media}
          image={book.photo}
          title="Contemplative Reptile"
        />
      </Link>
      <CardContent className={classes.root}>
        <Typography gutterBottom variant="h5" component="h2">
          {book.title}
        </Typography>
        <Typography variant="body2" color="textSecondary" component="p">
          {book.description}
        </Typography>
      </CardContent>

      <CardActions>
        <Link to={`/books/${book._id}`} style={{ textDecoration: "none" }}>
          <Button size="small" color="primary">
            Learn More
          </Button>
        </Link>
        <Button
          size="small"
          color="primary"
          onClick={() => onDeleteBook(book._id)}>
          Delete book
        </Button>
      </CardActions>
    </Card>
  )
}

export { Book }
