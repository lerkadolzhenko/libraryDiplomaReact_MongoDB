import React from "react"
import { Grid } from "@material-ui/core"
import Search from "../Search"
import SelectAmount from "../SelectAmount"

export default function Filter({ onSelectAmount, pageLimit, onSearchBook ,error}) {
  return (
    <form noValidate autoComplete="off" style={{ marginBottom: "15px" }}>
      <Grid container direction="row" justify="space-between" spacing={3}>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <SelectAmount onSelectAmount={onSelectAmount} pageLimit={pageLimit} />
        </Grid>
        <Grid item lg={3} md={4} sm={6} xs={12}>
          <Search onSearchBook={onSearchBook} error={error} />
        </Grid>
      </Grid>
    </form>
  )
}

export { Filter }
