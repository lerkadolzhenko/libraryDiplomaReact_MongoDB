import React from "react"
import "./AnimBook.css"

export default function AnimBook() {
  return (
    <>
      <span class="svg-animation">
        <svg viewBox="0 0 600 150" preserveAspectRatio="none">
          <use id="wave-addon1" class="animation-waves" xlinkHref="#wave-right" x="0" y="0"></use>
          <use id="wave-addon2" class="animation-waves" xlinkHref="#wave-right" x="0" y="50"></use>
          <use id="wave-addon3" class="animation-waves-updown" xlinkHref="#wave-right" x="0" y="30"></use>
          <defs>
            <path
              id="wave-right"
              d="M600,18c-65.1,1.4-56.4,119-150,119S365.1,19.4,300,18c-65.1,1.4-56.4,119-150,119S65.1,19.4,0,18v132h150h150h150h150V18z"
              fill="grey"
            />
          </defs>
        </svg>
      </span>
      <span class="svg-animation svg-animation180">
        <svg viewBox="0 0 600 150" preserveAspectRatio="none">
          <use id="wave-addon1" class="animation-waves" xlinkHref="#wave-right" x="0" y="0"></use>
          <use id="wave-addon2" class="animation-waves" xlinkHref="#wave-right" x="0" y="50"></use>
          <use id="wave-addon3" class="animation-waves-updown" xlinkHref="#wave-right" x="0" y="30"></use>
          <defs>
            <path
              id="wave-right"
              d="M600,18c-65.1,1.4-56.4,119-150,119S365.1,19.4,300,18c-65.1,1.4-56.4,119-150,119S65.1,19.4,0,18v132h150h150h150h150V18z"
              fill="grey"
            />
          </defs>
        </svg>
      </span>
    </>
  )
}
