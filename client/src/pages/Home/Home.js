import React, { useState, useCallback, useEffect } from "react"
import {
  Typography,
  Grid,
  makeStyles,
  Box,
  IconButton,
  Paper,
  Button,
} from "@material-ui/core"
import ImportContactsIcon from "@material-ui/icons/ImportContacts"
import Mail from "@material-ui/icons/Mail"
import Place from "@material-ui/icons/Place"
import LocalPhone from "@material-ui/icons/LocalPhone"
import { Link } from "react-router-dom"
import { useHttp } from "../../hooks/httpHook"
import AnimBook from "./components/book/AnimBook"

const useStyles = makeStyles((theme) => ({
  imageHome: {
    width: "100%",
    height: 400,
    objectFit: "cover",
  },
  space: {
    marginTop: 4,
    marginBottom: 4,
  },
  homeContacts: {
    color: "#1F1F1F",
    paddingTop: 30,
    paddingBottom: 30,
  },
  homeContactsMap: {
    width: "100%",
    height: "45vh",
  },
  mainFeaturedPost: {
    position: "relative",
    backgroundColor: theme.palette.grey[800],
    color: theme.palette.common.white,
    // marginBottom: theme.spacing(4),
    backgroundImage: "url(https://source.unsplash.com/random)",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
  },
  overlay: {
    position: "absolute",
    top: 0,
    bottom: 0,
    right: 0,
    left: 0,
    backgroundColor: "rgba(0,0,0,.3)",
  },
  mainFeaturedPostContent: {
    position: "relative",
    padding: theme.spacing(3),
    [theme.breakpoints.up("md")]: {
      padding: theme.spacing(6),
      paddingRight: 0,
    },
  },
  padding: {
    paddingTop: 30,
  },

}))

export default function Home() {
  const classes = useStyles()
  const { request, loading } = useHttp()
  const [singleBook, setBook] = useState({})

  const getBookId = useCallback(async () => {
    try {
      const data = await request(
        `/api/books/6187aedc1abc46544c10850e`,
        "GET",
        null
      )
      setBook(data)
      console.log("single" + data)
    } catch (e) {}
  }, [request])

  useEffect(() => {
    getBookId()
  }, [getBookId])

  const date = new Date(singleBook?.publishDate)

  return (
    <>
      <Paper className={classes.mainFeaturedPost}>
        <div className={classes.overlay} />
        <Grid container>
          <Grid item md={6}>
            <div className={classes.mainFeaturedPostContent}>
              <Typography
                component="h1"
                variant="h3"
                color="inherit"
                gutterBottom>
                {singleBook?.title}
              </Typography>
              <Typography variant="h5" color="inherit" paragraph>
                {singleBook?.description}
              </Typography>
              <Typography variant="h6" color="inherit" paragraph>
                {date.toDateString()}
              </Typography>
              <Link
                to={`/books/${singleBook?._id}`}
                style={{ textDecoration: "none" }}>
                <Button variant="outlined" color="primary">
                  Learn More
                </Button>
              </Link>
            </div>
          </Grid>
        </Grid>
      </Paper>
      <Box m={3} className={classes.padding}>
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="textPrimary"
          gutterBottom>
          Our features
        </Typography>
        <Grid
          className={classes.padding}
          container
          direction="row"
          justify="space-around"
          alignItems="center">
          <Grid item>
            <Typography variant="h4" color="inherit" paragraph align="center">
              1000+
            </Typography>
            <Typography variant="h5" color="inherit" paragraph align="center">
              books
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="h4" color="inherit" paragraph align="center">
              Comfortable
            </Typography>
            <Typography variant="h5" color="inherit" paragraph align="center">
              usage
            </Typography>
          </Grid>
          <Grid item>
            <Typography variant="h4" color="inherit" paragraph align="center">
              Everywhere
            </Typography>
            <Typography variant="h5" color="inherit" paragraph align="center">
              with you
            </Typography>
          </Grid>
        </Grid>
      </Box>
      <AnimBook />
      <Box m={3} className={classes.padding}>
        <Typography
          component="h1"
          variant="h2"
          align="center"
          color="textPrimary"
          gutterBottom>
          Contacts
        </Typography>
        <Grid className={classes.homeContacts} container>
          <Grid item xs={12} md={4}>
            <Typography variant="h6" color="inherit" paragraph>
              <IconButton color="inherit">
                <ImportContactsIcon />
              </IconButton>
              Welcome on Online Library
            </Typography>

            <Typography variant="h6" color="inherit" paragraph>
              <IconButton color="inherit">
                <LocalPhone />
              </IconButton>
              Telephone:
              <a href={`tel:+380955525742`}>+380955525742</a>
            </Typography>

            <Typography variant="h6" color="inherit" paragraph>
              <IconButton color="inherit">
                <Mail />
              </IconButton>
              E-mail:
              <a href={`mailto:valeriiadolzhenko@gmail.com`}>
                valeriiadolzhenko@gmail.com
              </a>
            </Typography>
          </Grid>

          <Grid item xs={12} md={8}>
            <iframe
              className={classes.homeContactsMap}
              title="Map"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2656.4155098105616!2d25.944563215653083!3d48.256377879233064!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47340f2ecf39331d%3A0xad9e50d9d97d5d39!2z0YPQuy4g0JLQvtGA0L7QsdC60LXQstC40YfQsCwg0KfQtdGA0L3QvtCy0YbRiywg0KfQtdGA0L3QvtCy0LjRhtC60LDRjyDQvtCx0LvQsNGB0YLRjCwgNTgwMDA!5e0!3m2!1sru!2sua!4v1591734358710!5m2!1sru!2sua"></iframe>
          </Grid>
        </Grid>
      </Box>
    </>
  )
}

export { Home }
