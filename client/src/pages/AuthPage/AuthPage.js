import React, { useState, useEffect, useContext } from "react"
import Avatar from "@material-ui/core/Avatar"
import Button from "@material-ui/core/Button"
import CssBaseline from "@material-ui/core/CssBaseline"
import TextField from "@material-ui/core/TextField"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined"
import Typography from "@material-ui/core/Typography"
import { makeStyles } from "@material-ui/core/styles"
import Container from "@material-ui/core/Container"
import { useHttp } from "../../hooks/httpHook"
import { useMessage } from "../../hooks/errorMessage"
import { AuthContext } from "../../context/AuthContext"

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

export const AuthPage = () => {
  const auth = useContext(AuthContext)
  const { loading, error, request, clearError } = useHttp()
  const classes = useStyles()
  const message = useMessage()
  const [form, setForm] = useState({
    email: "",
    password: "",
  })

  const changeHandler = (event) => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }
  useEffect(() => {
    // console.log(error)
    message(error)
    clearError()
  }, [error, message, clearError])

  const registerHandler = async () => {
    try {
      const data = await request("/api/auth/register", "POST", { ...form })
      message(data.message)
      console.log("data", data)
    } catch (e) {}
  }

  const loginHandler = async () => {
    try {
      const data = await request("/api/auth/login", "POST", { ...form })
      // message(data.message)
      auth.login(data.token, data.userId)
      console.log("data", data)
    } catch (e) {}
  }

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign in
        </Typography>
        <form className={classes.form} noValidate>
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email Address"
            name="email"
            autoComplete="email"
            autoFocus
            onChange={changeHandler}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label="Password"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={changeHandler}
          />

          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={loading}
            onClick={loginHandler}>
            Sign In
          </Button>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="secondary"
            className={classes.submit}
            onClick={registerHandler}
            disabled={loading}>
            Sign Up
          </Button>
        </form>
      </div>
    </Container>
  )
}
