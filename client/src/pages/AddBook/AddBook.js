import React, { useState, useEffect } from "react"
import { TextField, Box, Button, makeStyles } from "@material-ui/core"
import { useHttp } from "../../hooks/httpHook"
import { useMessage } from "../../hooks/errorMessage"
import { useHistory } from "react-router-dom"

const useStyles = makeStyles((theme) => ({
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}))

export const AddBook = () => {
  const history = useHistory()
  const { loading, error, request, clearError } = useHttp()
  const message = useMessage()
  const classes = useStyles()
  const now = new Date()
  const PublishDate = now.toDateString()
  const [form, setForm] = useState({
    title: "",
    description: "",
    pageCount: "",
    bookText: "",
    author: "",
    publishDate: PublishDate,
    photo: "",
  })

  const changeHandler = (event) => {
    setForm({ ...form, [event.target.name]: event.target.value })
  }
  useEffect(() => {
    console.log(error)
    message(error)
    clearError()
  }, [error, message, clearError])

  const onAddBook = async () => {
    try {
      const data = await request("api/books/addbook", "POST", { ...form })
      message(data.message)
      console.log("data", data)
      history.push(`/books`)
    } catch (e) {}
  }

  return (
    <Box m={3}>
      <form className={classes.form} noValidate>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="title"
          label="title"
          name="title"
          autoComplete="title"
          autoFocus
          onChange={changeHandler}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="photo"
          label="photo"
          type="photo"
          id="photo"
          autoComplete="current-photo"
          onChange={changeHandler}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="author"
          label="author"
          type="author"
          id="author"
          autoComplete="current-author"
          onChange={changeHandler}
        />

        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="description"
          label="description"
          type="description"
          id="description"
          autoComplete="current-description"
          onChange={changeHandler}
        />

        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="bookText"
          label="bookText"
          type="bookText"
          id="bookText"
          rows="4"
          multiline
          autoComplete="current-bookText"
          onChange={changeHandler}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          name="pageCount"
          label="pageCount"
          type="pageCount"
          id="pageCount"
          autoComplete="current-pageCount"
          onChange={changeHandler}
        />

        <Button
          type="submit"
          fullWidth
          variant="contained"
          color="secondary"
          className={classes.submit}
          disabled={loading}
          onClick={onAddBook}>
          Add book
        </Button>
      </form>
    </Box>
  )
}
