import React, { useCallback, useEffect, useState } from "react"
import {
  Typography,
  Button,
  Card,
  CardContent,
  CardActions,
  Box,
  CardHeader,
  CardMedia,
  makeStyles,
} from "@material-ui/core"
// import EditIcon from "@material-ui/icons/Edit"
import Loader from "../../components/Loader/Loader"
import { Link } from "react-router-dom"
import getSingleBook from "../../redux/singleBook/getSingleBook"
import { connect } from "react-redux"
import { useHttp } from "../../hooks/httpHook"

const useStyles = makeStyles((theme) => ({
  singleBookImg: {
    width: 380,
    height: 450,
    padding: 15,
  },
  singleBookCardHeader: {
    padding: 15,
  },
}))

export const SingleBook = (props) => {
  const classes = useStyles()
  const { request, loading } = useHttp()
  const [singleBook, setBook] = useState({})

  console.log("props.match.params" + props.match.params.id)
  const getBookId = useCallback(async () => {
    try {
      const data = await request(
        `/api/books/${props.match.params.id}`,
        "GET",
        null
      )
      setBook(data)
      console.log("single" + data)
    } catch (e) {}
  }, [request])

  useEffect(() => {
    getBookId()
  }, [getBookId])

  const date = new Date(singleBook.publishDate)

  return (
    <div>
      {loading ? (
        <Loader />
      ) : (
        <Box m={3}>
          <Card>
            <Box display="flex" flexDirection="row">
              <img
                src={singleBook.photo}
                className={classes.singleBookImg}
                alt={singleBook.title}
              />
              <div className={classes.singleBookCardHeader}>
                <Typography variant="h5" component="h2">
                  Title: {singleBook.title}
                </Typography>
               
                <Typography variant="subtitle1">
                  Description: {singleBook.description}
                </Typography>
                 <Typography color="textSecondary">
                  Author: {singleBook.author}
                </Typography>
                <Typography variant="caption" color="textSecondary">
                  Publish date: {date.toDateString()}
                </Typography>
              </div>
            </Box>
            <CardContent>
              <Typography variant="body2" component="p">
                Book : {singleBook.bookText}
              </Typography>
            </CardContent>
            <CardActions>
              <Link
                style={{
                  textDecoration: "none",
                  color: "#fff",
                  marginLeft: "45%",
                }}
                to="/books">
                <Button variant="contained" color="primary">
                  Go Back
                </Button>
              </Link>
            </CardActions>
          </Card>
        </Box>
      )}
    </div>
  )
}

const mapStateToProps = (state) => {
  return {
    singleBook: state.singleBook,
    isLoading: state.isLoading,
  }
}

const mapDispatchToProps = (dispach) => {
  return {
    onGetBook: (id) => dispach(getSingleBook(id)),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(SingleBook)
