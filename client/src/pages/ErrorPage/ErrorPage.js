import React from "react"
import { Typography, Button, Grid } from "@material-ui/core"
import { Link } from "react-router-dom"

export default function ErrorPage() {
  return (
    <Grid
      container
      direction="column"
      justify="center"
      alignItems="center"
      style={{ marginTop: "20vh" }}>
      <Typography variant="h2" gutterBottom>
        OOPS ! Page not found
      </Typography>
      <Link style={{ textDecoration: "none", color: "#fff" }}  variant="body2" to="/">
        <Button variant="contained" size="large" color="primary">
          Go Home
        </Button>
      </Link>
    </Grid>
  )
}

export { ErrorPage }
