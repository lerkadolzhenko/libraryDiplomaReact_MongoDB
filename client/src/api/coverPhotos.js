import client from "./client"

export const getCoverPhotos = () => client.get("/api/CoverPhotos")
export const getCoverPhoto= (id) => client.get(`/books/covers/${id}`)

export const postCoverPhoto = (coverPhoto) => client.post("/api/CoverPhotos", coverPhoto)

export const deleteCoverPhoto = (id) => client.delete(`/api/CoverPhotos/${id}`)

export const putCoverPhoto = (id, coverPhoto) => client.put(`/api/CoverPhotos/${id}`, coverPhoto)
