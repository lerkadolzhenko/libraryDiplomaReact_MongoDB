import Axios from "axios";

export const client = Axios.create({
  baseURL: "https://fakerestapi.azurewebsites.net"
})
export default client;