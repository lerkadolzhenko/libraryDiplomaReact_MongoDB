import client from "./client"

export const getBooks = () => client.get("/api/Books")
export const getBook = (id) => client.get(`/api/Books/${id}`)

export const postBook = (book) => client.post("/api/Books", book)

export const deleteBook = (id) => client.delete(`/api/Books/${id}`)

export const putBook = (id, book) => client.put(`/api/Books/${id}`, book)
