import React from "react"
import Pagination from "@material-ui/lab/Pagination"
import { Grid } from "@material-ui/core"

export default function BasicPagination({ onPageChange, totalPages }) {
  
  const [page, setPage] = React.useState(1)
  const handleChange = (event, value) => {
    setPage(value)
    onPageChange(value)
  }

  return (
    <Grid container direction="row" justify="center" alignItems="center" spacing={3}>
      <Grid item>
        <Pagination
          count={totalPages}
          color="primary"
          page={page}
          onChange={handleChange}
        />
      </Grid>
    </Grid>
  )
}

export { BasicPagination }
