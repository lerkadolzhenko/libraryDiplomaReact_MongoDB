import React from "react"
import {LinearProgress } from "@material-ui/core"

export default function Loader() {
  return (
      <div style={{marginTop: "50vh"}}>
      <LinearProgress />
      </div>
  )
}

export { Loader }
