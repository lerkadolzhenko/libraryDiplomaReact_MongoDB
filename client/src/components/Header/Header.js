import React, { useContext } from "react"
import { Button, AppBar, Toolbar, Typography, Grid } from "@material-ui/core"
import { Link ,useHistory} from "react-router-dom"
import { AuthContext } from "../../context/AuthContext"

export default function Header() {
  const history=useHistory()
  const auth = useContext(AuthContext)

  const logoutHandler = (event) => {
    event.preventDefault()
    auth.logout()
    history.push('/')
  }
  return (
    <div>
      <AppBar position="static">
        <Toolbar>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center">
            <Typography variant="h6">Online Library</Typography>
            <div>
              <Link style={{ textDecoration: "none", color: "#fff" }} to="/">
                <Button color="inherit">Home</Button>
              </Link>
              <Link
                style={{ textDecoration: "none", color: "#fff" }}
                to="/books">
                <Button color="inherit">Books</Button>
              </Link>
              <Link
                style={{ textDecoration: "none", color: "#fff" }}
                to="/addbook">
                <Button color="inherit">Add book</Button>
              </Link>
              <Link style={{ textDecoration: "none", color: "#fff" }} to="/">
                <Button color="inherit" onClick={logoutHandler}>
                  Logout
                </Button>
              </Link>
            </div>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  )
}

export { Header }
