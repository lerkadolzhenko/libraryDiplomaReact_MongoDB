import { createStore, applyMiddleware } from "redux"
import singleBookReducer from "./singleBook/singleBookReducer"
import thunk from "redux-thunk"

export const store = createStore(singleBookReducer, applyMiddleware(thunk))
