export const PENDING_SINGLE_BOOK = "PENDING_SINGLE_BOOK"
export const RECEIVE_SINGLE_BOOK = "RECEIVE_SINGLE_BOOK"
export const FAIL_GET_SINGLE_BOOK = "FAIL_GET_SINGLE_BOOK"

export const requestSingleBook = (loading) => {
  console.log('requestSingleBook')
  return {
    type: PENDING_SINGLE_BOOK,
    isLoading: loading,
  }
}

export const receiveSingleBook = (singleBook) => {
  console.log('receiveSingleBook')

  return {
    type: RECEIVE_SINGLE_BOOK,
    payload: {...singleBook},
  }
}

export const failGetSingleBook = (error) => {
  console.log('failGetSingleBook')

  return {
    type: FAIL_GET_SINGLE_BOOK,
    payload: {...error},
  }
}
