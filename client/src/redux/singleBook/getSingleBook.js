import {
  failGetSingleBook,
  receiveSingleBook,
  requestSingleBook,
} from "./actionCreators"
import { getBook } from "../../api/books"

export function getSingleBook(id) {
  return (dispatch, getState) => {
    dispatch(requestSingleBook(true))
    console.log("current state:", getState())
    getBook(id)
      .then((response) => {
        dispatch(receiveSingleBook(response.data))
        console.log(response.data)
      })
      .catch((error) => {
        dispatch(failGetSingleBook(error.message))
      })
  }
}

export default getSingleBook
