import {
  PENDING_SINGLE_BOOK,
  RECEIVE_SINGLE_BOOK,
  FAIL_GET_SINGLE_BOOK,
} from "./actionCreators"

const initialState = {
  isLoading: false,
  didInvalidate: false,
  singleBook: {},
}

export const singleBookReducer = (state = initialState, action) => {
  console.log("reducer", state, action)
  switch (action.type) {
    case PENDING_SINGLE_BOOK:
      console.log('PENDING_SINGLE_BOOK')

      return {
        ...state,
        isLoading: action.isLoading,
      }
    case RECEIVE_SINGLE_BOOK:
      console.log('RECEIVE_SINGLE_BOOK')
      return {
        ...state,
        isLoading: false,
        error: null,
        singleBook: action.payload,
      }
    case FAIL_GET_SINGLE_BOOK:
      console.log('FAIL_GET_SINGLE_BOOK')

      return {
        ...state,
        isLoading: false,
        error: action.payload,
      }
    default:
      return state
  }
}


export default singleBookReducer
