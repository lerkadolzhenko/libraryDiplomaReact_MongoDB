import React from "react"
import { BrowserRouter } from "react-router-dom"
import { useRoutes } from "./routes"
import { AuthContext } from "./context/AuthContext"
import { useAuth } from "./hooks/authHook"
import { store } from "./redux/store"
import { Provider } from "react-redux"

function App() {
  const { token, login, logout, userId } = useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated)
  return (
    <AuthContext.Provider
      value={{ isAuthenticated, token, login, logout, userId }}>
    
      <Provider store={store}>
        <div className="App">
          <BrowserRouter>{routes}</BrowserRouter>
        </div>{" "}
      </Provider>
    </AuthContext.Provider>
  )
}

export default App
