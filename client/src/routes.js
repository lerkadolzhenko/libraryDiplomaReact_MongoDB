import React from "react"
import { Switch, Route, Redirect } from "react-router-dom"
import { LinksPage } from "./pages/LinksPage/LinksPage"
import { CreatePage } from "./pages/CreatePage/CreatePage"
import { DetailsPage } from "./pages/DetailsPage/DetailsPage"
import { AuthPage } from "./pages/AuthPage/AuthPage"
import ErrorPage from "./pages/ErrorPage"
import Header from "./components/Header"
import Home from "./pages/Home"
import Books from "./pages/Books"
import SingleBook from "./pages/SingleBook/SingleBook"
import { AddBook } from "./pages/AddBook/AddBook"
import "./routes.css"
import Footer from "./components/Footer/Footer"

export const useRoutes = (isAuthenticated) => {
  if (isAuthenticated) {
    return (
      <>
        <Header />
        <div className="content">
          <Switch>
            <Route exact path="/detail/:id" component={DetailsPage} />
            <Route exact path="/home" component={Home} />
            <Route exact path="/books" component={Books} />
            <Route exact path="/books/:id" component={SingleBook} />
            <Route exact path="/addbook" component={AddBook} />
            <Redirect to="/home" />
          </Switch>
        </div>
        <Footer />
      </>
      // <Switch>
      //   <Route path="/links" exact>
      //     <LinksPage />
      //   </Route>
      //   <Route path="/create" exact>
      //     <CreatePage />
      //   </Route>
      //   <Route path="/detail/:id">
      //     <DetailsPage />
      //   </Route>
      //   <Redirect to="/create" />
      // </Switch>
    )
  }

  return (
    <Switch>
      <Route path="/" exact>
        <AuthPage />
      </Route>
      <Redirect to="/" />
    </Switch>
  )
}
